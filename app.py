"""Environment spawner."""

import datetime
import subprocess
import string
import random
import time

import streamlit as st

from streamlit_cookies_controller import CookieController


# ---------------------------------------
# Page Setup
st.set_page_config(page_title="Env Spawner", page_icon="✨", layout="centered")
st.markdown(
    """
    <style>
        .reportview-container {
            margin-top: -2em;
        }
        #MainMenu {visibility: hidden;}
        .stDeployButton {display:none;}
        footer {visibility: hidden;}
        #stDecoration {display:none;}
    </style>
""",
    unsafe_allow_html=True,
)


controller = CookieController()

st.title("🔬 Lab IA 🔬")
st.markdown(
    """
Génère un environnement à la demande pour le lab IA!
"""
)

cookie = controller.get("env_spawner")

# ---------------------------------------
# Env Spawner
if cookie is None:
    st.markdown("Pour créer un nouvel environnement, clique simplement sur le bouton ci-dessous.")

    if st.button("Générer l'environnement", type="primary", use_container_width=True):
        st.markdown("*Attention le démarrage de l'environnement peut prendre quelques secondes.*")

        unique_name = "".join(
            random.choice(string.ascii_uppercase + string.digits) for _ in range(12)
        ).lower()
        env_name = f"env-{unique_name}"
        try:
            with st.spinner('Construction en cours...'):
                result = subprocess.run(
                    ["./scripts/create.sh", env_name],
                    check=True,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                )
            controller.set(
                "env_spawner",
                env_name,
                expires=datetime.datetime.now() + datetime.timedelta(hours=24),
            )
            st.markdown(""":green[Environnement généré avec succès !]""")

            time.sleep(2) # On laisse 2s pour laisser le temps à l'utilisateur de lire le message (et aussi d'enregistrer le cookie ...)
            st.rerun()
        except subprocess.CalledProcessError as e:
            st.markdown(
                f"""
    :red[Erreur lors de la génération de l'environnement {env_name}. Ressayez!]

    ```
    {e.stderr.decode('utf-8')}
    ```"""
            )

if cookie is not None:
    env_name = cookie
    st.markdown(
        f"""
        :green[Environnement en cours d'exécution]

        Informations de connexion:

        * URL vscode: [https://{env_name}-vscode.lab-ia.cloud-sp.eu/](https://{env_name}-vscode.lab-ia.cloud-sp.eu/)
        * URL webapp: [https://{env_name}-webapp.lab-ia.cloud-sp.eu/](https://{env_name}-webapp.lab-ia.cloud-sp.eu/)
        """
    )

    if st.button("Arrêter l'environnement", type="primary", use_container_width=True):
        try:
            with st.spinner('Suppression en cours...'):
                result = subprocess.run(
                    ["./scripts/destroy.sh", env_name],
                    check=True,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                )
            controller.remove("env_spawner")
            st.markdown(""":green[Environnement supprimé avec succès !]""")

            time.sleep(2) # On laisse 2s pour laisser le temps à l'utilisateur de lire le message (et aussi de supprimer le cookie ...)
            st.rerun()
        except subprocess.CalledProcessError as e:
            st.markdown(
                f"""
                :red[Erreur lors de la suppression de l'environnement {env_name}. Ressayez!]

                ```
                {e.stderr}
                ```"""
            )
    else:
        st.markdown(
            "*Attention l'arrêt peut prendre plusieurs secondes, merci de ne pas quitter la page lorsque vous arrêtez votre environnement.*"
        )