#!/bin/sh

set -eu

export ENVNAME=$1

echo " --> Suppression de l'environnement $ENVNAME"

kubectl delete ns $ENVNAME
