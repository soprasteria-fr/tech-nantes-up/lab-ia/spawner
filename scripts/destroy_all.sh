#!/bin/sh

set -eu

NB_LABS=$1

echo " --> Suppression de $NB_LABS environnements"

for i in `seq 1 $NB_LABS`; do
    ./destroy.sh "lab-$i"
done
