#!/bin/sh

set -eu

export ENVNAME=$1

echo " --> Création de l'environnement $ENVNAME"

kubectl create ns $ENVNAME || true && \
envsubst < env.yml | kubectl -n $ENVNAME apply -f -
