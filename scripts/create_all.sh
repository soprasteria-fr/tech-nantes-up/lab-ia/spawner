#!/bin/sh

set -eu

NB_LABS=$1

echo " --> Création de $NB_LABS environnements"

for i in `seq 1 $NB_LABS`; do
    ./create.sh "lab-$i"
done
