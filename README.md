# Spawner

Déploiement des environnements à la demande.

## Déploiement d'un environnement

1. Se déplacer dans le dossier `envs`.
2. Jouer le script `create.sh`:

```bash
./scripts/create.sh <nom_environnement>
```

## Suppression d'un environnement

1. Se déplacer dans le dossier `envs`.
2. Jouer le script `destroy.sh`:

```bash
./scripts/destroy.sh <nom_environnement>
```
